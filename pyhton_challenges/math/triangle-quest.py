"""
https://www.hackerrank.com/challenges/python-quest-1/problem?isFullScreen=true

Found solution in discussion. Integer division by 9 gives the expected progression.
"""

if __name__ == '__main__':
    for i in range(1, int(input())):
        print(i * (10 ** i) // 9)
