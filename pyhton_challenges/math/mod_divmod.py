"""
https://www.hackerrank.com/challenges/python-mod-divmod/problem?isFullScreen=true
"""


def main():
    a, b = int(input()), int(input())
    result = divmod(a, b)
    print(result[0], result[1], result, sep='\n')


if __name__ == '__main__':
    main()
