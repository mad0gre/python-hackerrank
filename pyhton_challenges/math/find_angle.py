"""
https://www.hackerrank.com/challenges/find-angle/problem?isFullScreen=true
"""
import math


def main():
    ab = int(input())
    bc = int(input())
    hyp = math.hypot(ab, bc)
    deg = round(math.degrees(math.asin(ab / hyp)))
    print(u'{}{}'.format(deg, chr(176)))


if __name__ == '__main__':
    main()
