"""
https://www.hackerrank.com/challenges/polar-coordinates/problem?isFullScreen=true
"""

import cmath


def main():
    imaginary = complex(input())
    print(cmath.sqrt(imaginary.real ** 2 + imaginary.imag ** 2).real)
    print(cmath.phase(imaginary))


if __name__ == '__main__':
    main()
