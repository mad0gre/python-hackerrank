"""
https://www.hackerrank.com/challenges/python-integers-come-in-all-sizes/problem?isFullScreen=true
"""


def main():
    a, b, c, d = (int(input()) for _ in range(4))
    print(a ** b + c ** d)


if __name__ == '__main__':
    main()
