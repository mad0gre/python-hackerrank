"""
https://www.hackerrank.com/challenges/python-quest-1/problem?isFullScreen=true

Found solution in discussion. Multiplication by 11 n times gives the expected progression.

A much nicer way to do it would be as follows:

def main():
    arr = [*range(1, int(input()) + 1)]
    for i in range(len(arr)):
        sub = arr[:i+1]
        comp = sub[:i][::-1]
        print(*(sub + comp), sep='')


if __name__ == '__main__':
    main()

"""

if __name__ == '__main__':
    for i in range(1, int(input()) + 1):
        print((10 ** i // 9) ** 2)
