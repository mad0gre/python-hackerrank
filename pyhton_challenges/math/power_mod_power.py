"""
https://www.hackerrank.com/challenges/python-power-mod-power/problem?isFullScreen=true
"""


def main():
    a, b, m = int(input()), int(input()), int(input())
    print(pow(a, b), pow(a, b, m), sep='\n')


if __name__ == '__main__':
    main()
