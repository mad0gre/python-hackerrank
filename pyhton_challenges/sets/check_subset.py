"""
https://www.hackerrank.com/challenges/py-check-subset/problem?isFullScreen=true
"""


def main():
    test_cases = int(input())
    for _ in range(test_cases):
        test_case()


def test_case():
    _, set_a, _, set_b = input(), set(input().split()), input(), set(input().split())
    print(len(set_a.difference(set_b)) == 0)


if __name__ == '__main__':
    main()
