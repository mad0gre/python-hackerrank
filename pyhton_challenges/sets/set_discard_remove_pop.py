"""
https://www.hackerrank.com/challenges/py-set-discard-remove-pop/problem
"""


def execute_set_method(numbers, method_name, *args):
    try:
        method = getattr(numbers, method_name)
        method(*args)
    except KeyError:
        pass


def main():
    _ = int(input())
    numbers = set([int(i) for i in input().split()])
    commands_size = int(input())
    for i in range(commands_size):
        command = input().split()
        params = [int(p) for p in command[1:]]
        execute_set_method(numbers, command[0], *params)
    print(sum(numbers))


if __name__ == '__main__':
    main()
