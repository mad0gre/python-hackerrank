"""
https://www.hackerrank.com/challenges/no-idea/problem
"""


def calculate_happiness(input_numbers, liked, disliked):
    happiness = 0
    for input_number in input_numbers:
        if input_number in liked:
            happiness += 1
        if input_number in disliked:
            happiness -= 1

    return happiness


def parse_input():
    return tuple(map(int, input().split()))


if __name__ == '__main__':
    n, m = parse_input()
    numbers = parse_input()
    liked_numbers = set(parse_input())
    disliked_numbers = set(parse_input())
    print(calculate_happiness(numbers, liked_numbers, disliked_numbers))
