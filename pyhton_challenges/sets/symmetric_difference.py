"""
https://www.hackerrank.com/challenges/symmetric-difference/problem
"""


def print_symmetric_diff(s1, s2):
    sorted_res = sorted(s1 ^ s2)
    for i in sorted_res:
        print(i)


if __name__ == '__main__':
    m = int(input())
    m_list = set(map(int, input().split()))
    n = int(input())
    n_list = set(map(int, input().split()))
    print_symmetric_diff(m_list, n_list)
