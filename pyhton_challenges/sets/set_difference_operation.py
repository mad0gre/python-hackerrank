"""
https://www.hackerrank.com/challenges/py-set-difference-operation/problem?isFullScreen=true
"""


def main():
    input()
    english = set([int(e) for e in input().split()])
    input()
    french = set([int(f) for f in input().split()])
    print(len(english - french))


if __name__ == '__main__':
    main()
