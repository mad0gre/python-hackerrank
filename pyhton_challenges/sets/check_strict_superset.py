"""
https://www.hackerrank.com/challenges/py-check-strict-superset/problem?isFullScreen=true
"""


def main():
    superset = set(input().split())
    result = True
    for _ in range(int(input())):
        subset = set(input().split())
        result = superset.issuperset(subset) and len(superset) > len(subset)
        if not result:
            break
    print(result)


if __name__ == '__main__':
    main()
