"""
https://www.hackerrank.com/challenges/py-set-mutations/problem?isFullScreen=true
"""


def retrieve_command_tuple():
    command = input().split()[0]
    param = set([int(el) for el in input().split()])
    return command, param


def main():
    input()
    original = set([int(el) for el in input().split()])
    num_ops = int(input())
    for _ in range(num_ops):
        command, param = retrieve_command_tuple()
        method = getattr(original, command)
        method(param)
    print(sum(original))


if __name__ == '__main__':
    main()
