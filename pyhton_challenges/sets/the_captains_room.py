"""
https://www.hackerrank.com/challenges/py-the-captains-room/problem?isFullScreen=true
"""


def main():
    k = int(input())
    rooms = [int(room) for room in input().split()]
    candidate_rooms = set()
    distinct_rooms = set()
    for room in rooms:
        if room not in distinct_rooms:
            distinct_rooms.update([room])
            candidate_rooms.update([room])
        else:
            candidate_rooms.difference_update([room])
    print(candidate_rooms.pop())


if __name__ == '__main__':
    main()
