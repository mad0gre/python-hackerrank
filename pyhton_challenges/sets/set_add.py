"""
https://www.hackerrank.com/challenges/py-set-add/problem
"""

if __name__ == '__main__':
    print(len(set([input() for _ in range(int(input()))])))
