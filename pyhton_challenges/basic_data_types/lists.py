"""
https://www.hackerrank.com/challenges/python-lists/problem
"""


if __name__ == '__main__':
    n = int(input())
    arr = []

    for _ in range(n):
        try:
            cmd = input().split()
            getattr(arr, cmd[0])(*tuple(list(map(int, cmd[1:]))))
        except:
            print(arr)
