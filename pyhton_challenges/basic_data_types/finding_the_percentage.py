"""
https://www.hackerrank.com/challenges/finding-the-percentage/problem
"""

import functools

if __name__ == '__main__':
    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        scores = list(map(float, line))
        student_marks[name] = scores
    query_name = input()

    scores = student_marks[query_name]
    sum_scores = functools.reduce(lambda x, y: x + y, scores)
    print("%.2f" % (sum_scores / len(scores)))
