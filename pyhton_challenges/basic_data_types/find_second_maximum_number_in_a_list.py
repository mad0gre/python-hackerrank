"""
https://www.hackerrank.com/challenges/find-second-maximum-number-in-a-list/problem
"""


def runner_up_score(scores):
    champs = -101
    runner_up = -101
    for x in scores:
        if x > champs:
            runner_up = champs
            champs = x
        elif (x < champs) and (x > runner_up):
            runner_up = x
    return runner_up


if __name__ == '__main__':
    n = int(input())
    arr = map(int, input().split())
    print('{}'.format(runner_up_score(arr)))
