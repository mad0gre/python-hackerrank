"""
https://www.hackerrank.com/challenges/nestedt/problem
"""

if __name__ == '__main__':
    lowest = float('inf')
    second_lowest = float('inf')
    lowest_names = []
    second_lowest_names = []

    for _ in range(int(input())):
        name = input()
        score = float(input())

        if score < lowest:
            second_lowest = lowest
            second_lowest_names = lowest_names
            lowest = score
            lowest_names = [name]
        elif score == lowest:
            lowest_names.append(name)
        else:
            if score < second_lowest:
                second_lowest = score
                second_lowest_names = [name]
            elif score == second_lowest:
                second_lowest_names.append(name)

    second_lowest_names.sort()
    for name in second_lowest_names:
        print(name)
