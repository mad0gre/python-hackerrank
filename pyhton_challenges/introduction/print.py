"""
https://www.hackerrank.com/challenges/python-print/problem
"""


def inflate(n):
    factor = 1
    result = 0

    for i in range(n, 0, -1):
        result += i * factor
        factor *= 10 ** order(i)

    return result


def order(n):
    div = n // 10
    res = 1
    while div > 0:
        res += 1
        div //= 10

    return res


if __name__ == '__main__':
    num = int(input())
    print("%d" % (inflate(num)))
