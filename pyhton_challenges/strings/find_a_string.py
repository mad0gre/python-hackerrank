"""
https://www.hackerrank.com/challenges/find-a-string/problem
"""


def count_substring(s, sub_s):
    c = 0
    max_pos = len(s) - len(sub_s) + 1
    for i in range(max_pos):
        if s[i:i + len(sub_s)] == sub_s:
            c += 1

    return c


if __name__ == '__main__':
    string = input().strip()
    sub_string = input().strip()

    count = count_substring(string, sub_string)
    print(count)
