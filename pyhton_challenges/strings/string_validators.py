"""
https://www.hackerrank.com/challenges/string-validators/problem
"""


def validate(s):
    has_alnum, has_alpha, has_digit, has_lower, has_upper = (False,) * 5
    for i in range(len(s)):
        if s[i].isalnum():
            has_alnum = True

            if not has_alpha and s[i].isalpha():
                has_alpha = True
            if not has_digit and s[i].isdigit():
                has_digit = True
            if not has_lower and s[i].islower():
                has_lower = True
            if not has_upper and s[i].isupper():
                has_upper = True

        if has_alnum and has_alpha and has_digit and has_lower and has_upper:
            break

    print(has_alnum)
    print(has_alpha)
    print(has_digit)
    print(has_lower)
    print(has_upper)


if __name__ == '__main__':
    s = input()
    validate(s)
