"""
https://www.hackerrank.com/challenges/designer-door-mat/problem
"""


def print_mat(n, m):
    base_pattern = '.|.'
    filler = '-'
    message = 'WELCOME'

    for i in range(0, n // 2):
        line = base_pattern * (1 + 2 * i)
        print(line.center(m, filler))

    print(message.center(m, filler))

    for i in range(n // 2, 0, -1):
        line = base_pattern * (2 * i - 1)
        print(line.center(m, filler))


if __name__ == '__main__':
    n, m = map(int, input().split())
    print_mat(n, m)
