"""
https://www.hackerrank.com/challenges/python-string-formatting/problem
"""


def print_formatted(number):
    pad = len(bin(n)[2:])
    for num in range(1, number + 1):
        n_bin = bin(num)[2:]
        n_oct = oct(num)[2:]
        n_hex = hex(num).upper()[2:]
        msg = "{1:>{0}} {2:>{0}} {3:>{0}} {4:>{0}}".format(pad, num, n_oct, n_hex, n_bin)
        print(msg)


if __name__ == '__main__':
    n = int(input())
    print_formatted(n)
