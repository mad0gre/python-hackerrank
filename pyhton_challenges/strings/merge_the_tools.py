"""
https://www.hackerrank.com/challenges/merge-the-tools/problem
"""


def merge_the_tools(s, size):
    chars = list(s)
    for i in range(0, len(chars), size):
        part = chars[i:i+size]
        found_chars = set()
        result = []
        for char in part:
            if char not in found_chars:
                found_chars.add(char)
                result.append(char)
        print(''.join(result))


if __name__ == '__main__':
    string, k = input(), int(input())
    merge_the_tools(string, k)
