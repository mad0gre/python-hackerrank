"""
https://www.hackerrank.com/challenges/text-wrap/problem
"""


def wrap(s, max):
    res = list(s)

    for i in range(len(res) // max + 1, 0, -1):
        res.insert(i * max, '\n')

    return ''.join(res)


if __name__ == '__main__':
    string, max_width = input(), int(input())
    result = wrap(string, max_width)
    print(result)
