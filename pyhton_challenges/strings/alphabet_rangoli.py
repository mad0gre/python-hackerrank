"""
https://www.hackerrank.com/challenges/alphabet-rangoli/problem
"""

# ASCII code for 'a'.
ASCII_BASE = 97


def print_rangoli(size):
    # Generate lines to create top triangle of Rangoli (including middle line).
    lines = []
    for i in range(1, size + 1):
        line = generate_line_arr(i, size)
        line = [chr(ascii_code) for ascii_code in line]
        lines.append(line)

    # Length of the middle line of Rangoli. Used to center all other lines.
    length = len(lines[-1]) * 2 - 1

    # Bottom triangle of Rangoli just mirrors the top triangle.
    lines[len(lines):] = lines[-2::-1]

    # Print lines by adding separators and centralizing.
    for line in lines:
        print('-'.join(line).center(length, '-'))


def generate_line_arr(line, size):

    # Line is generated with base ASCII code on the borders.
    # The further away from centre, less elements on the line.
    # Create "left side" the line, including middle element.
    base = ASCII_BASE + size
    line_arr = []
    for i in range(base - line, base):
        line_arr.append(i)

    # Mirror left side of line into right side.
    if len(line_arr) > 1:
        line_arr[:0] = line_arr[-1:0:-1]

    return line_arr


if __name__ == '__main__':
    n = int(input())
    print_rangoli(n)
