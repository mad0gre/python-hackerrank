"""
https://www.hackerrank.com/challenges/swap-case/problem
"""


def swap_case(s):
    return ''.join([swap_char(c) for c in s])


def swap_char(c):
    if c[0].isupper():
        return c[0].lower()
    else:
        return c[0].upper()


if __name__ == '__main__':
    s = input()
    result = swap_case(s)
    print(result)
