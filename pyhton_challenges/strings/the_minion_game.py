"""
https://www.hackerrank.com/challenges/the-minion-game/problem
"""

VOWELS = set('AEIOU')


def minion_game(string):
    stuart, kevin = 0, 0

    for i, c in enumerate(string):
        if c in VOWELS:
            kevin += len(string) - i
        else:
            stuart += len(string) - i

    if stuart > kevin:
        print("Stuart", stuart)
    elif kevin > stuart:
        print("Kevin", kevin)
    else:
        print("Draw")


if __name__ == '__main__':
    s = input()
    minion_game(s)
