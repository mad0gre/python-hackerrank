"""
https://www.hackerrank.com/challenges/capitalize/problem
"""

import os


def capitalize(string):
    names = string.split(" ")
    cap_names = []
    for name in names:
        cap_names.append(name.capitalize())

    return " ".join(cap_names)


if __name__ == '__main__':
    s = input()
    result = capitalize(s)

    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    fptr.write(result + '\n')
    fptr.close()
